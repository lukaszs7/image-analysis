package szmolke.io;

import javafx.util.Pair;
import szmolke.algorithm.KNN;
import szmolke.common.KeyPoint;
import szmolke.gui.Controller;

import java.io.*;
import java.util.*;

/**
 * Created by Łukasz on 04.06.2016.
 */
public class SiftFileReader {

    public static ArrayList<KeyPoint> readFile(String path) {
        try (Scanner scanner = new Scanner(new BufferedReader(new FileReader(new File(path)))).useLocale(Locale.US)) {
            double x;
            double y;
            int featuresCount = scanner.nextInt();
            int pointsCount = scanner.nextInt();
            ArrayList<KeyPoint> keyPoints = new ArrayList<>(pointsCount);
            for (int i = 0; i < pointsCount; i++) {
                List<Integer> features = new ArrayList<>(featuresCount);
                x = scanner.nextDouble();
                y = scanner.nextDouble();
                for (int j = 0; j < 3; j++) {
                    scanner.next();
                }
                for (int j = 0; j < featuresCount; j++) {
                    features.add(scanner.nextInt());
                }
                keyPoints.add(new KeyPoint(x, y, features));
            }
            return keyPoints;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Pair<KeyPoint,KeyPoint>> getKeyPointPairs(ArrayList<KeyPoint> keyPoint1, ArrayList<KeyPoint> keyPoint2, String fileName) {
        File pairsFile = new File(Controller.EXTRACT_FEATURES_PATH + "\\pairs\\" + fileName + ".pairs");
        List<Pair<KeyPoint,KeyPoint>> pairs;
        if(pairsFile.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(pairsFile)))){
                pairs = (List<Pair<KeyPoint,KeyPoint>>) ois.readObject();
                return pairs;
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            pairs = KNN.evaluateKNN(keyPoint1, keyPoint2);
            try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(pairsFile)))) {
                oos.writeObject(pairs);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return pairs;
        }
        return null;
    } 
}
