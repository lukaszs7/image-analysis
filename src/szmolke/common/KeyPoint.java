package szmolke.common;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Łukasz on 04.06.2016.
 */
public class KeyPoint implements Serializable {
    private double x;
    private double y;
    private List<Integer> features;

    public KeyPoint(double x, double y, List<Integer> features) {
        this.x = x;
        this.y = y;
        this.features = features;
    }

    public double distanceEvaluate(KeyPoint keyPoint) {
        return Math.pow(x - keyPoint.getX(), 2) + Math.pow(y - keyPoint.getY(), 2);
    }

    public double featuresEvaluate(KeyPoint keyPoint) {
        double result = 0;
        for (int i = 0; i < features.size(); i++) {
            result += Math.pow(features.get(i) - keyPoint.getFeatures().get(i), 2);
        }
        return result;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public List<Integer> getFeatures() {
        return features;
    }

    public void setFeatures(List<Integer> features) {
        this.features = features;
    }
}
