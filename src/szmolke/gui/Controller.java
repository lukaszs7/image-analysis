package szmolke.gui;

import javafx.collections.FXCollections;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.util.Pair;
import szmolke.algorithm.*;
import szmolke.common.KeyPoint;
import szmolke.io.SiftFileReader;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable{
    public static final String EXTRACT_FEATURES_PATH = "E:\\Studia\\VI semestr\\Sztuczna inteligencja\\zadanie4\\extract_features\\";
    private static final String[] IMAGES = {
            "img1", "img2", "img3", "img4", "img5", "img6", "img7", "img8", "img9", "img10", "img11", "img12", "img13", "img14", "img15", "img16"
    };
    private static final String EXTENSION_PNG = ".png";
    private static final String EXTENSION_SIFT = ".haraff.sift";
    private static final int POINT_DIAMETER = 5;
    public static final double RANSAC_DISTANCE_LESS_THAN_FACTOR = 0.3;
    public static final double RANSAC_DISTANCE_GREATER_THAN_FACTOR = 0.01;

    public static double KNN_NEIGHBORHOOD_FACTOR = 0.1; //liczność sąsiedztwa
    public static double COHESION_FILTER_FACTOR = 0.25; //próg spójności
    public static int RANSAC_MAX_ITERATIONS = 5000;
    public static int RANSAC_MAX_ERROR = 500;
    public static int RANSAC_MAX_ITERATIONS_HEURISTIC = 5000;

    public ChoiceBox image1Box, image2Box;
    public Button equalButton;
    public RadioButton radioCohesion, radioRansacAffine, radioRansacPerspective, radioHeuristicNone, radioHeuristicDistance;
    public Canvas canvas;
    public TextField knnFilterRatio, knnFilterMinRatio, ransacIterations, ransacIterationsHeuristic, ransacMaxError;
    public TextArea textAreaResults;
    public ToggleGroup typeAlgorithm;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initButtons();
        initChoiceBoxes();
        initTextFileds();
        initToggleGroups();
        setRadioButtonsDisablity(true, radioHeuristicNone, radioHeuristicDistance);
    }

    private void initToggleGroups() {
        typeAlgorithm.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if(radioCohesion.isSelected()) {
                setRadioButtonsDisablity(true, radioHeuristicNone, radioHeuristicDistance);
            } else if (radioRansacAffine.isSelected()) {
                setRadioButtonsDisablity(false, radioHeuristicNone, radioHeuristicDistance);
            } else if (radioRansacPerspective.isSelected()) {
                setRadioButtonsDisablity(false, radioHeuristicNone, radioHeuristicDistance);
            }
        });
    }

    private RansacHeuristic getTypeHeuristic() {
        if(radioHeuristicNone.isSelected()) {
            return RansacHeuristic.NONE;
        } else if (radioHeuristicDistance.isSelected()) {
            return RansacHeuristic.EVAL_DISTANCE;
        } else {
            return null;
        }
    }

    private AlgorithmType getAlgorithmType() {
        if(radioCohesion.isSelected()) {
            return AlgorithmType.COHESION;
        } else if (radioRansacAffine.isSelected()) {
            return AlgorithmType.RANSAC_AFFINE;
        } else if (radioRansacPerspective.isSelected()) {
            return AlgorithmType.RANSAC_PERSPECTIVE;
        } else {
            return null;
        }
    }

    private void setRadioButtonsDisablity(boolean disable, RadioButton... radioButtons) {
        for (int i = 0; i < radioButtons.length; i++) {
            radioButtons[i].setDisable(disable);
        }
    }

    private void initTextFileds() {
        knnFilterRatio.setText(String.valueOf(KNN_NEIGHBORHOOD_FACTOR));
        knnFilterMinRatio.setText(String.valueOf(COHESION_FILTER_FACTOR));
        ransacIterations.setText(String.valueOf(RANSAC_MAX_ITERATIONS));
        ransacIterationsHeuristic.setText(String.valueOf(RANSAC_MAX_ITERATIONS_HEURISTIC));
        ransacMaxError.setText(String.valueOf(RANSAC_MAX_ERROR));
    }

    private void initButtons() {
        equalButton.setOnMouseClicked(event -> {
            setInitVariables();
            AlgorithmType algType = getAlgorithmType();
            RansacHeuristic ransacHeu = getTypeHeuristic();
            String img1 = (String)image1Box.getSelectionModel().getSelectedItem();
            String img2 = (String)image2Box.getSelectionModel().getSelectedItem();
            startAlgorithm(algType, ransacHeu, img1, img2);
        });
    }

    private void setInitVariables() {
        KNN_NEIGHBORHOOD_FACTOR = Double.parseDouble(knnFilterRatio.getText());
        COHESION_FILTER_FACTOR = Double.parseDouble(knnFilterMinRatio.getText());
        RANSAC_MAX_ITERATIONS = Integer.parseInt(ransacIterations.getText());
        RANSAC_MAX_ITERATIONS_HEURISTIC = Integer.parseInt(ransacIterationsHeuristic.getText());
        RANSAC_MAX_ERROR = Integer.parseInt(ransacMaxError.getText());
    }

    private void initChoiceBoxes() {
        image1Box.setItems(FXCollections.observableArrayList(IMAGES));
        image2Box.setItems(FXCollections.observableArrayList(IMAGES));

        if(IMAGES.length > 1) {
            image1Box.getSelectionModel().select(0);
            image2Box.getSelectionModel().select(1);
        }
    }

    private void startAlgorithm(AlgorithmType algorithmType, RansacHeuristic heuristic, String img1, String img2) {
        ArrayList<KeyPoint> keyPoints1 = SiftFileReader.readFile(EXTRACT_FEATURES_PATH + img1 + EXTENSION_PNG + EXTENSION_SIFT);
        ArrayList<KeyPoint> keyPoints2 = SiftFileReader.readFile(EXTRACT_FEATURES_PATH + img2 + EXTENSION_PNG + EXTENSION_SIFT);
        List<Pair<KeyPoint, KeyPoint>> pairsKeyPoints = SiftFileReader.getKeyPointPairs(keyPoints1, keyPoints2, "knn_"+img1+"_"+img2);

        Image image1 = new Image(new File(EXTRACT_FEATURES_PATH + img1 + EXTENSION_PNG).toURI().toString());
        Image image2 = new Image(new File(EXTRACT_FEATURES_PATH + img2 + EXTENSION_PNG).toURI().toString());
        double size = (image1.getWidth() + image1.getHeight() + image2.getWidth() + image2.getHeight()) / 4;
        IFilter filterAlg = getAppropriateAlgorithm(algorithmType, heuristic, pairsKeyPoints, size);
        long time1 = System.currentTimeMillis();
        List<Pair<KeyPoint, KeyPoint>> filtered = filterAlg.getFilteredPairs();
        long evalTime = System.currentTimeMillis() - time1;

        fillCanvasWithImages(image1, image2);
        fillCanvasWithPoints(filtered, image1.getWidth());
        fillCanvasWithLines(filtered, image1.getWidth());
        textAreaResults.setText("Czas wykonania: "+evalTime+"[ms] | Pary kluczowe: "+pairsKeyPoints.size()+" | Po filtracji: "+filtered.size()+" | PK1: "+keyPoints1.size()+" | PK2: "+keyPoints2.size());
    }

    private void fillCanvasWithLines(List<Pair<KeyPoint, KeyPoint>> filtered, double width) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setLineWidth(1);
        gc.setStroke(new Color(0, 1, 0, 0.4));
        filtered.stream()
                .forEach(pair -> {
                    KeyPoint kp1 = pair.getKey();
                    KeyPoint kp2 = pair.getValue();
                    gc.strokeLine(kp1.getX(), kp1.getY(), kp2.getX() + width, kp2.getY());
                });
    }

    private void fillCanvasWithPoints(List<Pair<KeyPoint, KeyPoint>> filtered, double image1Width) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BLUE);
        filtered.stream()
                .forEach(pair -> {
                    KeyPoint kp1 = pair.getKey();
                    KeyPoint kp2 = pair.getValue();
                    gc.fillOval(kp1.getX() - POINT_DIAMETER/2, kp1.getY() - POINT_DIAMETER/2, POINT_DIAMETER, POINT_DIAMETER);
                    gc.fillOval(kp2.getX() - POINT_DIAMETER/2 + image1Width, kp2.getY() - POINT_DIAMETER/2, POINT_DIAMETER, POINT_DIAMETER);
                });
    }

    private void fillCanvasWithImages(Image image1, Image image2) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        canvas.setWidth(image1.getWidth()+image2.getWidth());
        canvas.setHeight(Math.max(image1.getHeight(), image2.getHeight()));
        double widthImg1 = image1.getWidth();
        gc.drawImage(image1, 0, 0);
        gc.drawImage(image2, widthImg1, 0);
    }

    private IFilter getAppropriateAlgorithm(AlgorithmType algorithmType, RansacHeuristic heuristic, List<Pair<KeyPoint, KeyPoint>> pairsKeyPoints, double size) {
        IFilter algorithm = null;
        switch (algorithmType) {
            case COHESION:
                algorithm = new Cohesion(pairsKeyPoints);
                break;
            case RANSAC_AFFINE:
                algorithm = new RansacAffine(pairsKeyPoints, heuristic, size, RANSAC_MAX_ITERATIONS, RANSAC_MAX_ERROR);
                break;
            case RANSAC_PERSPECTIVE:
                algorithm = new RansacPerspective(pairsKeyPoints, heuristic, size, RANSAC_MAX_ITERATIONS, RANSAC_MAX_ERROR);
                break;
        }
        return algorithm;
    }
}
