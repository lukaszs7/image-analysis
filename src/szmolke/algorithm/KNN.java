package szmolke.algorithm;

import javafx.util.Pair;
import szmolke.common.KeyPoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Łukasz on 04.06.2016.
 */
public class KNN {
    public static List<Pair<KeyPoint, KeyPoint>> evaluateKNN(List<KeyPoint> points1, List<KeyPoint> points2) {
        List<Pair<KeyPoint, KeyPoint>> keyPoints = new ArrayList<>();
        KeyPoint keyPoint1;
        KeyPoint keyPoint2;
        for (int i = 0; i < points1.size(); i++) {
            keyPoint1 = points1.get(i);
            keyPoint2 = getNearestNeighbor(keyPoint1, points2);
            if(keyPoint1.equals(getNearestNeighbor(keyPoint2, points1))) {
                keyPoints.add(new Pair<>(keyPoint1, keyPoint2));
            }
        }
        return keyPoints;
    }

    public static KeyPoint getNearestNeighbor(KeyPoint keypoint, List<KeyPoint> keyPoints) {
        KeyPoint nearestNeighbor = null;
        double nearestValue = Double.MAX_VALUE;
        for (int i = 0; i < keyPoints.size(); i++) {
            double current = keypoint.featuresEvaluate(keyPoints.get(i));
            if(current < nearestValue) {
                nearestNeighbor = keyPoints.get(i);
                nearestValue = current;
            }
        }
        return nearestNeighbor;
    }

    public static List<KeyPoint> getNearestKeypoints(KeyPoint keyPoint, ArrayList<KeyPoint> keyPoints, int knnCount) {
        return keyPoints.stream()
                    .sorted((kp1, kp2) -> (int)(keyPoint.distanceEvaluate(kp1) - keyPoint.distanceEvaluate(kp2)))
                    .limit(knnCount)
                    .collect(Collectors.toList());
    }
}
