package szmolke.algorithm;

/**
 * Created by Łukasz on 04.06.2016.
 */
public enum AlgorithmType {
    COHESION, RANSAC_AFFINE, RANSAC_PERSPECTIVE
}
