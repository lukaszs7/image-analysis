package szmolke.algorithm;

import Jama.Matrix;
import javafx.util.Pair;
import szmolke.common.KeyPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz on 05.06.2016.
 */
public class RansacAffine implements IFilter, IRansac {
    private List<Pair<KeyPoint, KeyPoint>> pairs;
    private RansacHeuristic ransacHeuristic;
    private double imgSize;
    private int maxIterations;
    private int maxError;

    public RansacAffine(List<Pair<KeyPoint, KeyPoint>> pairs, RansacHeuristic ransacHeuristic, double imgSize, int maxIterations, int maxError) {
        this.pairs = pairs;
        this.ransacHeuristic = ransacHeuristic;
        this.imgSize = imgSize;
        this.maxIterations = maxIterations;
        this.maxError = maxError;
    }

    @Override
    public double modelError(Matrix model, Pair<KeyPoint, KeyPoint> pair) {
        double[][] doublesMatrix = new double[][] {
                {pair.getKey().getX()},
                {pair.getKey().getY()},
                {1}
        };
        Matrix matrix = model.times(new Matrix(doublesMatrix));
        KeyPoint keyPoint = new KeyPoint(matrix.get(0, 0), matrix.get(1, 0), null);
        double distance = pair.getValue().distanceEvaluate(keyPoint);
        return distance;
    }

    @Override
    public Matrix calculateModel(List<Pair<KeyPoint, KeyPoint>> samples) {
        Matrix model = null;
        KeyPoint x1 = samples.get(0).getKey();
        KeyPoint u1 = samples.get(0).getValue();
        KeyPoint x2 = samples.get(1).getKey();
        KeyPoint u2 = samples.get(1).getValue();
        KeyPoint x3 = samples.get(2).getKey();
        KeyPoint u3 = samples.get(2).getValue();

        double[][] doublesMatrix1 = new double[][] {
            {x1.getX(), x1.getY(), 1, 0, 0, 0},
            {x2.getX(), x2.getY(), 1, 0, 0, 0},
            {x3.getX(), x3.getY(), 1, 0, 0, 0},
            {0, 0, 0, x1.getX(), x1.getY(), 1},
            {0, 0, 0, x2.getX(), x2.getY(), 1},
            {0, 0, 0, x3.getX(), x3.getY(), 1}
        };

        double[][] doublesMatrix2 = new double[][] {
            {u1.getX()},
            {u2.getX()},
            {u3.getX()},
            {u1.getY()},
            {u2.getY()},
            {u3.getY()}
        };
        Matrix matrix1 = new Matrix(doublesMatrix1);
        Matrix matrix2 = new Matrix(doublesMatrix2);
        if(matrix1.det() != 0) {
            Matrix result = matrix1.inverse().times(matrix2);
            double[][] resultMatrix = new double[][] {
                {result.get(0, 0), result.get(1, 0), result.get(2, 0)},
                {result.get(3, 0), result.get(4, 0), result.get(5, 0)},
                {0, 0, 1}
            };
            model = new Matrix(resultMatrix);
        }
        return model;
    }

    @Override
    public int getPairsCount() {
        return 3;
    }

    @Override
    public List<Pair<KeyPoint, KeyPoint>> getFilteredPairs() {
        List<Pair<KeyPoint, KeyPoint>> bestFiltered = null;
        List<Pair<KeyPoint, KeyPoint>> filtered;
        List<Pair<KeyPoint, KeyPoint>> samples;
        Matrix model;
        int bestScore = 0;
        int score;
        double error;
        for (int i = 0; i < maxIterations; i++) {
            model = null;
            filtered = new ArrayList<>();
            while(model == null) {
                samples = getSamples(pairs, ransacHeuristic, imgSize);
                model = calculateModel(samples);
            }
            score = 0;
            for(Pair<KeyPoint, KeyPoint> pair : pairs) {
                error = modelError(model, pair);
                if(error < maxError) {
                    score++;
                    filtered.add(pair);
                }
            }
            if(score > bestScore) {
                bestScore = score;
                bestFiltered = filtered;
            }
        }
        return bestFiltered;
    }
}
