package szmolke.algorithm;

import Jama.Matrix;
import javafx.util.Pair;
import szmolke.common.KeyPoint;
import szmolke.gui.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz on 05.06.2016.
 */
public interface IRansac {
    double modelError(Matrix model, Pair<KeyPoint, KeyPoint> pair);
    Matrix calculateModel(List<Pair<KeyPoint, KeyPoint>> samples);
    int getPairsCount();

    default List<Pair<KeyPoint,KeyPoint>> getSamples(List<Pair<KeyPoint, KeyPoint>> pairs, RansacHeuristic ransacHeuristic, double imgSize) {
        List<Pair<KeyPoint,KeyPoint>> samples = null;
        switch (ransacHeuristic) {
            case NONE:
                samples = getSamplesByRandom(pairs);
                break;
            case EVAL_DISTANCE:
                samples = getSamplesHeuristicByDistance(pairs, imgSize);
                break;
        }
        return samples;
    }

    default List<Pair<KeyPoint,KeyPoint>> getSamplesHeuristicByDistance(List<Pair<KeyPoint, KeyPoint>> pairs, double imgSize) {
        List<Pair<KeyPoint,KeyPoint>> copyPairs = new ArrayList<>(pairs);
        List<Pair<KeyPoint,KeyPoint>> samples = new ArrayList<>();
        double r = imgSize * Controller.RANSAC_DISTANCE_GREATER_THAN_FACTOR;
        double R = imgSize * Controller.RANSAC_DISTANCE_LESS_THAN_FACTOR;
        Pair<KeyPoint, KeyPoint> oldPair = copyPairs.remove((int)(Math.random()*copyPairs.size()));
        Pair<KeyPoint, KeyPoint> newPair;
        Pair<KeyPoint, KeyPoint> oldPair1;
        Pair<KeyPoint, KeyPoint> oldPair2;
        samples.add(oldPair);
        boolean isDistanceCorrect = false;
        int iterations = 0;
        int samplesCount = 1;

        while(samplesCount < getPairsCount()) {
            newPair = copyPairs.get((int)(Math.random()*copyPairs.size()));
            if(samplesCount == 1) {
                oldPair = samples.get(0);
                isDistanceCorrect = isDistanceCorrect(oldPair, newPair, r, R);
            } else if (samplesCount == 2) {
                oldPair = samples.get(0);
                oldPair1 = samples.get(1);
                isDistanceCorrect = isDistanceCorrect(oldPair, newPair, r, R) && isDistanceCorrect(oldPair1, newPair, r, R);
            } else if (samplesCount == 3) {
                oldPair = samples.get(0);
                oldPair1 = samples.get(1);
                oldPair2 = samples.get(2);
                isDistanceCorrect = isDistanceCorrect(oldPair, newPair, r, R) && isDistanceCorrect(oldPair1, newPair, r, R)&& isDistanceCorrect(oldPair2, newPair, r, R);
            }

            if(iterations > Controller.RANSAC_MAX_ITERATIONS_HEURISTIC) {
                isDistanceCorrect = true;
            }

            if(isDistanceCorrect) {
                copyPairs.remove(newPair);
                samples.add(newPair);
                samplesCount++;
            }
            iterations++;
        }
        return samples;
    }

    default List<Pair<KeyPoint,KeyPoint>> getSamplesByRandom(List<Pair<KeyPoint, KeyPoint>> pairs) {
        List<Pair<KeyPoint,KeyPoint>> copyPairs = new ArrayList<>(pairs);
        List<Pair<KeyPoint,KeyPoint>> samples = new ArrayList<>();
        Pair<KeyPoint, KeyPoint> pair;
        for (int i = 0; i < getPairsCount(); i++) {
            pair = copyPairs.remove((int)(Math.random()*copyPairs.size()));
            samples.add(pair);
        }
        return samples;
    }

    default boolean isDistanceCorrect(Pair<KeyPoint, KeyPoint> pair1, Pair<KeyPoint, KeyPoint> pair2, double r, double R) {
        double kp1KeyX = pair1.getKey().getX(); //x1
        double kp1KeyY = pair1.getKey().getY(); //y1
        double kp1ValueX = pair1.getValue().getX(); //u1
        double kp1ValueY = pair1.getValue().getY(); //v1
        double kp2KeyX = pair2.getKey().getX(); //x2
        double kp2KeyY = pair2.getKey().getY(); //y2
        double kp2ValueX = pair2.getValue().getX(); //u2
        double kp2ValueY = pair2.getValue().getY(); //v2

        return  Math.pow(r, 2) < (Math.pow(kp1KeyX - kp2KeyX, 2) + Math.pow(kp1KeyY - kp2KeyY, 2)) &&
                Math.pow(R, 2) > (Math.pow(kp1KeyX - kp2KeyX, 2) + Math.pow(kp1KeyY - kp2KeyY, 2)) &&
                Math.pow(r, 2) < (Math.pow(kp1ValueX - kp2ValueX, 2) + Math.pow(kp1ValueY - kp2ValueY, 2)) &&
                Math.pow(R, 2) > (Math.pow(kp1ValueX - kp2ValueX, 2) + Math.pow(kp1ValueY - kp2ValueY, 2));
    }
}
