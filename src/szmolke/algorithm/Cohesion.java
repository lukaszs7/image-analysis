package szmolke.algorithm;

import javafx.util.Pair;
import szmolke.common.KeyPoint;
import szmolke.gui.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz on 05.06.2016.
 */
public class Cohesion implements IFilter {
    private List<Pair<KeyPoint, KeyPoint>> pairs;

    public Cohesion(List<Pair<KeyPoint, KeyPoint>> pairs) {
        this.pairs = pairs;
    }

    @Override
    public List<Pair<KeyPoint, KeyPoint>> getFilteredPairs() {
        List<Pair<KeyPoint, KeyPoint>> filtered = new ArrayList<>();
        int knnNeighborhoodCount = (int) (pairs.size() * Controller.KNN_NEIGHBORHOOD_FACTOR);
        int minCohesionFilter = (int) (knnNeighborhoodCount * Controller.COHESION_FILTER_FACTOR);

        for(int i = 0; i < pairs.size(); i++) {
            List<KeyPoint> nearestKP1 = KNN.getNearestKeypoints(pairs.get(i).getKey(), getListFromPairs(true, pairs), knnNeighborhoodCount);
            List<KeyPoint> nearestKP2 = KNN.getNearestKeypoints(pairs.get(i).getValue(), getListFromPairs(false, pairs), knnNeighborhoodCount);

            int counter = 0;
            for (int j = 0; j < nearestKP1.size(); j++) {
                int index = indexOfValue(pairs, nearestKP1.get(j));
                if(nearestKP2.contains(pairs.get(index).getValue())) {
                    counter++;
                }
            }

            if(counter >= minCohesionFilter) {
                filtered.add(pairs.get(i));
            }
        }
        return filtered;
    }

}
