package szmolke.algorithm;

import javafx.util.Pair;
import szmolke.common.KeyPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Łukasz on 05.06.2016.
 */
public interface IFilter {
    List<Pair<KeyPoint, KeyPoint>> getFilteredPairs();
    default ArrayList<KeyPoint> getListFromPairs(boolean keyPoint1, List<Pair<KeyPoint, KeyPoint>> pairs) {
        ArrayList<KeyPoint> keyPoints = new ArrayList<>(pairs.size());
        for (int i = 0; i < pairs.size(); i++) {
            if(keyPoint1) {
                keyPoints.add(pairs.get(i).getKey());
            } else {
                keyPoints.add(pairs.get(i).getValue());
            }
        }
        return keyPoints;
    }
    default int indexOfValue(List<Pair<KeyPoint, KeyPoint>> pairs, KeyPoint pairKey) {
        int index = -1;
        for(int i = 0; i < pairs.size(); i++) {
            if(pairs.get(i).getKey().equals(pairKey)) {
                index = i;
                break;
            }
        }
        return index;
    }
}
